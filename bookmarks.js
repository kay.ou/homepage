// Note: having length != 4 will mess with layout based on how the site is styled
const bookmarks = [
  {
    title: "Daily",
    links: [
      { name: "Gmail", url: "inbox.google.com" },
      { name: "OneDrive", url: "onedrive.live.com" },
      { name: "GDrive", url: "drive.google.com" },
    ],
  },
  {
    title: "Develop",
    links: [
      { name: "Stackoverflow", url: "stackoverflow.com" },
      { name: "GitHub", url: "github.com" },
      { name: "Gitlab", url: "gitlab" },
    ],
  },
  {
    title: "Media",
    links: [
      { name: "Youtube", url: "youtube.com" },
      { name: "Douyin", url: "douyin.com" },
      { name: "TikTok", url: "tiktok" },
    ],
  },
  {
    title: "Social",
    links: [
      { name: "Twitter", url: "twitter.com" },
      { name: "Facebook", url: "facebook.com" },
      { name: "Instagram", url: "instagram.com" },
    ],
  },
];